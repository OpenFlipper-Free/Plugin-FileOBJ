include (plugin)

openflipper_plugin (PYTHONINTERFACE
                    TYPES POLYMESH TRIANGLEMESH
                    OPT_TYPES BSPLINECURVE BSPLINESURFACE)
