/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/




#define FILEOBJPLUGIN_C

#include "FileOBJ.hh"

#include <OpenMesh/Core/Utils/color_cast.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>


//-----------------------------------------------------------------------------------------------------

template< class MeshT >
bool FileOBJPlugin::writeMaterial(QString _filename, MeshT& _mesh, int _objId )
{
  bool optionFaceColors         = false;
  bool optionFaceColorsOverride = false;
  bool optionColorAlpha         = false;
  bool optionTextures           = false;
  bool optionCopyTextures       = false;
  bool optionCreateTexFolder    = false;

  // check options
  if ( !OpenFlipper::Options::savingSettings() && saveOptions_ != 0) {

      optionColorAlpha         = saveAlpha_->isChecked();
      optionTextures           = saveTextures_->isChecked();
      optionCopyTextures       = saveCopyTextures_->isChecked();
      optionCreateTexFolder    = saveCreateTexFolder_->isChecked();
  }
  // \TODO Fetch options from ini states if dialog box is not available

  std::fstream matStream( _filename.toStdString().c_str(), std::ios_base::out );

  if ( !matStream ){

    emit log(LOGERR, tr("writeMaterial : cannot not open file %1").arg(_filename) );
    return false;
  }

  // \TODO Implement setting of all colors (diffuse, ambient and specular)
  // There's only diffuse colors so far
  OpenMesh::Vec4f c;

  materials_.clear();

  // Prepare materials ( getMaterial handles a list that is set up by this call)
  for (auto f_it : _mesh.faces()){
    getMaterial(_mesh, f_it, _objId);
  }

  //write the materials
  for(MaterialList::iterator it = materials_.begin(); it != materials_.end(); ++it) {
      Material& mat = (*it).second;
      matStream << "newmtl " << mat << '\n';
      matStream << "Ka 0.5000 0.5000 0.5000" << '\n';
      ACG::Vec3f c = mat.Kd();
      matStream << "Kd " << c[0] << " " << c[1] << " " << c[2] << '\n';
      if(optionColorAlpha) {
          matStream << "Tr " << mat.Tr() << '\n';
      }
      matStream << "illum 1" << '\n';

      // Write out texture info
      if(optionTextures && mat.has_Texture()) {
          if(optionCopyTextures) {
              // Use file path in target folder (relative)
              QFileInfo file(mat.map_Kd().c_str());
              if(optionCreateTexFolder) {
                  QFileInfo materialFilename(_filename);

                  matStream << "map_Kd " << materialFilename.baseName().toStdString() << "_textures" << QDir::separator().toLatin1()
                      << file.fileName().toStdString() << '\n';
              } else {
                  matStream << "map_Kd " << file.fileName().toStdString() << '\n';
              }
          } else {
              // Use original file path
              matStream << "map_Kd " << mat.map_Kd() << '\n';
          }
      }

      matStream << '\n';
  }

  matStream.close();

  return true;
}

//-----------------------------------------------------------------------------------------------------

template< class MeshT >
Material& FileOBJPlugin::getMaterial(MeshT& _mesh, const OpenMesh::FaceHandle& _fh, int _objId)
{
  bool optionFaceColors         = false;
  bool optionFaceColorsOverride = false;
  bool optionColorAlpha         = false;

  if ( !OpenFlipper::Options::savingSettings() && saveOptions_ != 0) {
    optionFaceColors         = saveFaceColor_->isChecked();
    optionFaceColorsOverride = saveFaceColorOverride_->isChecked();
    optionColorAlpha         = saveAlpha_->isChecked();
  }
  // \TODO Fetch options from ini states if dialog box is not available

  OpenMesh::Vec4f c = _mesh.color( _fh );

  if ( optionFaceColors && !optionFaceColorsOverride ) {

    // If one of the entries is out of range, assume uninitialized color and set to default
    // Ugly hack to ensure that materials will only be generated if initialized
    if ( c[0] > 255.0 || c[1] > 255.0 || c[2] > 255.0 || c[0] < 0.0 || c[1] < 0.0 || c[2] < 0.0 || std::isnan(c[0]) || std::isnan(c[1]) || std::isnan(c[2]) ) {
      c[0] = 155.0;
      c[1] = 155.0;
      c[2] = 155.0;
      c[3] = 1.0;

      ++materialErrors_;

      // If we have too many errors, we always return the first material
      // Except, if the override option is set.
      if (materialErrors_ > 10)
        return (*materials_.begin()).second;

    }

  }

  // First off, try to fetch texture index of current face/object...
    if(!textureIndexPropFetched_) {
        emit textureIndexPropertyName(_objId, textureIndexPropertyName_);
        textureIndexPropFetched_ = true;
    }

    int texIndex = -1;
    OpenMesh::FPropHandleT< int > texture_index_property;
    if ( _mesh.get_property_handle(texture_index_property, textureIndexPropertyName_.toStdString()) ) {
        texIndex = _mesh.property(texture_index_property, _fh);
    } else if ( _mesh.get_property_handle(texture_index_property, "f:textureindex") ) {
        texIndex = _mesh.property(texture_index_property, _fh);
    } else if(_mesh.has_face_texture_index()) {
        texIndex = _mesh.texture_index(_fh);
    } else {
        QString texName;
        emit getCurrentTexture(_objId, texName);
        if(texName != "NONE")
          emit textureIndex(texName, _objId, texIndex);
    }

    QString filename;
    bool hasTexture = false;

    if(texIndex != -1) {

        // Search for texture index in local map
        std::map<int,QString>::iterator it = texIndexFileMap_.find(texIndex);

        if(it != texIndexFileMap_.end()) {
            // We already know this file
            filename = (*it).second;
            hasTexture = true;
        } else {
            // A new texture file has been found
            QString texName;
            emit textureName(_objId, texIndex, texName);

            if(texName != "NOT_FOUND") {
                emit textureFilename( _objId, texName, filename );
                // => Add to local map
                texIndexFileMap_.insert(std::pair<int,QString>(texIndex, filename));
                hasTexture = true;
            }
        }
    }

    for (MaterialList::iterator it = materials_.begin(); it != materials_.end(); ++it) {

        // No texture has been found
        if(!hasTexture) {
            // ... just look for diffuse color in materials list
            if(((*it).second).Kd() == ACG::Vec3f(c[0], c[1], c[2]) &&
                ((optionColorAlpha && ((*it).second).Tr() == c[3]) || !optionColorAlpha))
                return (*it).second;
        } else {
            // Texture has been found, look for both, matching texture and color
            QString mKd(((*it).second).map_Kd().c_str());
            if((((*it).second).Kd() == ACG::Vec3f(c[0], c[1], c[2]) &&
                ((optionColorAlpha && ((*it).second).Tr() == c[3]) || !optionColorAlpha)) &&
                (filename == mKd && ((*it).second).map_Kd_index() == texIndex))
                return (*it).second;
        }
    }

    // If not found, add new material(s)
    Material mat;
    // Set diffuse color
    mat.set_Kd(c[0], c[1], c[2]);
    // Add transparency if available
    if(optionColorAlpha) mat.set_Tr(c[3]);
    mat.material_number(materials_.size());
    // Set texture info
    if(hasTexture)
        mat.set_map_Kd(filename.toStdString(), texIndex);

    materials_.insert(std::make_pair(QString("Material%1").arg(mat.material_number()).toStdString(), mat));
    MaterialList::iterator it = materials_.end();
    --it;
    return (*it).second;
}




//-----------------------------------------------------------------------------------------------------

template< class MeshT >
bool FileOBJPlugin::writeMesh(std::ostream& _out, QString _filename, MeshT& _mesh, int _objId){

  unsigned int i, nV, idx;
  ACG::Vec3d v, n;
  Vec2f t(0.0f,0.0f);
  typename MeshT::VertexHandle vh;
  bool useMaterial = false;
  OpenMesh::Vec4f c;

  bool optionFaceColors         = false;
  bool optionVertexNormals      = false;
  bool optionVertexTexCoords    = true;
  bool optionTextures           = false;
  bool optionCopyTextures       = false;
  bool optionCreateTexFolder    = false;

  QFileInfo fi(_filename);

  // check options
  if ( !OpenFlipper::Options::savingSettings() && saveOptions_ != 0) {
    optionFaceColors         = saveFaceColor_->isChecked();
    optionVertexNormals      = saveNormals_->isChecked();
    optionVertexTexCoords    = saveTexCoords_->isChecked();
    optionTextures           = saveTextures_->isChecked();
    optionCopyTextures       = saveCopyTextures_->isChecked();
    optionCreateTexFolder    = saveCreateTexFolder_->isChecked();
    _out.precision(savePrecision_->value());
  };
  // \TODO Fetch options from ini states if dialog box is not available

  //create material file if needed
  if ( optionFaceColors || optionTextures ){

    QString matFile = fi.absolutePath() + QDir::separator() + fi.baseName() + ".mtl";

    useMaterial = writeMaterial(matFile, _mesh, _objId);
  }

  // Header
  _out << "# " << _mesh.n_vertices() << " vertices, ";
  _out << _mesh.n_faces() << " faces" << '\n';

  // Material file
  if (useMaterial &&  optionFaceColors )
    _out << "mtllib " << fi.baseName().toStdString() << ".mtl" << '\n';

  // Store indices of vertices in a map such that
  // they can easily be referenced for face definitions
  // later on
  std::map<typename MeshT::VertexHandle, int> vtMapV;

  int cf = 1;
  // vertex data (point, normals, texcoords)
  for (i=0, nV=_mesh.n_vertices(); i<nV; ++i)
  {
    vh = typename MeshT::VertexHandle(i);
    v  = _mesh.point(vh);
    n  = _mesh.normal(vh);

    if ( _mesh.has_vertex_texcoords2D() && !_mesh.has_halfedge_texcoords2D() )
      t  = _mesh.texcoord2D(vh);

    // Write out vertex coordinates
    _out << "v " << v[0] <<" "<< v[1] <<" "<< v[2] << '\n';

    // Write out vertex coordinates
    if ( optionVertexNormals)
      _out << "vn " << n[0] <<" "<< n[1] <<" "<< n[2] << '\n';

    // Write out vertex texture coordinates
    if ( optionVertexTexCoords && _mesh.has_vertex_texcoords2D() && !_mesh.has_halfedge_texcoords2D()) {
      _out << "vt " << t[0] <<" "<< t[1] << '\n';
      vtMapV.insert(std::pair<typename MeshT::VertexHandle, int>(vh, cf));
      cf++;
    }
  }

  // Store indices of vertex coordinate (in obj-file)
  // in map such that the corresponding halfedge
  // can easily be found later on
  std::map<typename MeshT::HalfedgeHandle, int> vtMap;

  // If mesh has halfedge tex coords, write them out instead of vertex texcoords
  if(optionVertexTexCoords && _mesh.has_halfedge_texcoords2D()) {
      int count = 1;
      for ( auto f_it : _mesh.faces() ) {
          for(auto fh_it : f_it.halfedges()) {
              typename MeshT::TexCoord2D t = _mesh.texcoord2D(fh_it);
              _out << "vt " << t[0] << " " << t[1] << '\n';
              vtMap.insert(std::pair<typename MeshT::HalfedgeHandle, int>(fh_it, count));
              count++;
          }
      }
  }

  Material lastMat;

  // we do not want to write separators if we only write vertex indices
  bool vertexOnly =     !(optionVertexTexCoords && _mesh.has_halfedge_texcoords2D())
                    &&  !(optionVertexTexCoords && !_mesh.has_halfedge_texcoords2D() && _mesh.has_vertex_texcoords2D())
                    &&  !(optionVertexNormals);

  for ( auto f_it : _mesh.faces() ){

    if (useMaterial && optionFaceColors) {

        Material& material = getMaterial(_mesh, f_it, _objId);

        // If we are ina a new material block, specify in the file which material to use
        if(lastMat.material_number() != material.material_number() ) {
            _out << "usemtl " << material << '\n';
            lastMat = material;
        }
    }

    _out << "f";

    // Write out face information
    for(auto fh_it : f_it.halfedges()) {

        // Write vertex index
        idx = fh_it.to().idx() + 1;
        _out << " " << idx;

        if (!vertexOnly) {

          // Write separator
          _out << "/" ;

          if ( optionVertexTexCoords ) {
            // Write vertex texture coordinate index
            if ( optionVertexTexCoords && _mesh.has_halfedge_texcoords2D()) {
              // Refer to halfedge texture coordinates
              typename std::map<typename MeshT::HalfedgeHandle, int>::iterator it = vtMap.find(fh_it);
              if(it != vtMap.end())
                _out  << (*it).second;
            } else if (optionVertexTexCoords && !_mesh.has_halfedge_texcoords2D() && _mesh.has_vertex_texcoords2D()) {
              // Refer to vertex texture coordinates
              typename std::map<typename MeshT::VertexHandle, int>::iterator it = vtMapV.find(_mesh.to_vertex_handle(fh_it));
              if(it != vtMapV.end())
                _out  << (*it).second;
            }
          }

          // Write vertex normal index
          if ( optionVertexNormals ) {
            // Write separator
            _out << "/" ;

            _out << idx;
          }
        }
    }

    _out << '\n';
  }

  // Copy texture files (if demanded)
  if(optionCopyTextures) {
      // Only test existence of folder once
      // (for multiple textures)
      bool testedOnce = false;
      for(MaterialList::iterator it = materials_.begin(); it != materials_.end(); ++it) {
          Material& mat = (*it).second;

          if(!mat.has_Texture()) continue;

          QImage img(mat.map_Kd().c_str());
          QFileInfo img_f(mat.map_Kd().c_str());

          if(img.isNull()) {
              // Something happened wrong
              emit log(LOGERR, tr("An error occurred when trying to copy a texture file."));
              continue;
          } else {
              if(optionCreateTexFolder) {
                  // Create folder
                  QDir dir(fi.absolutePath());
                  if(!testedOnce && dir.exists(fi.absolutePath() + QDir::separator() + fi.baseName() + "_textures")) {
                      emit log(LOGERR, tr("The specified target folder already contains a subfolder called textures. Skipping!"));
                      continue;
                  } else {
                      dir.mkdir(fi.baseName() + "_textures");
                      img.save(fi.absolutePath() + QDir::separator() + fi.baseName() + "_textures" + QDir::separator() + img_f.fileName());
                      testedOnce = true;
                  }

              } else {
                  img.save(fi.absolutePath() + QDir::separator() + img_f.fileName());
              }
          }
      }
  }

  materials_.clear();
  materialErrors_ = 0;
  texIndexFileMap_.clear();
  textureIndexPropFetched_ = false;

  return true;
}




