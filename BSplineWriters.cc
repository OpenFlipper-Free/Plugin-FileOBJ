/*===========================================================================*\
 *                                                                           *
 *                             OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
\*===========================================================================*/



#include "FileOBJ.hh"


#ifdef ENABLE_BSPLINECURVE_SUPPORT
bool FileOBJPlugin::writeCurve(std::ostream& _out, QString _filename, BSplineCurve* _curve )
{
  if ( !OpenFlipper::Options::savingSettings() && saveOptions_ != 0) {
    _out.precision(savePrecision_->value());
  }

  _out << "# " << _filename.toStdString() << "\n";

  // save control points (coordinates)
  for (uint i = 0; i < _curve->n_control_points(); ++i){
    ACG::Vec3d cp = _curve->get_control_point(i);
    _out << "v " << cp[0] << " " << cp[1] << " " << cp[2] << "\n";
  }

  _out << "cstype bspline\n";
  _out << "deg " << _curve->degree() << "\n";
  _out << "g " << _filename.toStdString() << "\n";

  // save control polygon
  _out << "curv " << _curve->get_knot(0) << " " << _curve->get_knot(_curve->n_knots()-1) << " ";
  // save control point indices
  for (unsigned int i = 0; i < _curve->n_control_points(); ++i)
    _out << i+1 << " "; // obj enumerates the cps starting with 1
  _out << "\n";

  _out << "parm u ";
  // save knotvector
  for (unsigned int i = 0; i < _curve->n_knots(); ++i)
    _out << _curve->get_knot(i) << " ";
  _out << "\n";

  _out << "end";

  return true;
}
#endif

#ifdef ENABLE_BSPLINESURFACE_SUPPORT
bool FileOBJPlugin::writeSurface(std::ostream& _out, QString _filename, BSplineSurface* _surface ){
  if ( !OpenFlipper::Options::savingSettings() && saveOptions_ != 0) {
    _out.precision(savePrecision_->value());
  }

  _out << "# " << _filename.toStdString() << "\n";

  // save control net (coordinates)
  unsigned int num_cp_m = _surface->n_control_points_m();
  unsigned int num_cp_n = _surface->n_control_points_n();

  for (unsigned int i = 0; i < num_cp_m; ++i)
  {
    for (unsigned int j = 0; j < num_cp_n; ++j)
    {
      ACG::Vec3d cp = (*_surface)(i,j);
      _out << "v " << cp[0] << " " << cp[1] << " " << cp[2] << "\n";
    }
  }

  _out << "cstype bspline\n";
  _out << "deg " << _surface->degree_m() << " " << _surface->degree_n() << "\n";
  _out << "g " << _filename.toStdString() << "\n";

  // save control polygon
  _out << "surf " << _surface->get_knot_m(0) << " " << _surface->get_knot_m(_surface->n_knots_m()-1) << " "
                  << _surface->get_knot_n(0) << " " << _surface->get_knot_n(_surface->n_knots_n()-1) << " ";

  // save control point indices
  for (unsigned int j = 0; j < num_cp_n; ++j)
    for (unsigned int i = 0; i < num_cp_m; ++i)
      _out << (i*num_cp_n) + j+1 << " "; // obj enumerates the cps starting with 1

  _out << "\n";

  _out << "parm u ";
  // save knotvector in m direction
  for (unsigned int i = 0; i < _surface->n_knots_m(); ++i)
    _out << _surface->get_knot_m(i) << " ";
  _out << "\n";

  _out << "parm v ";
  // save knotvector in n direction
  for (unsigned int i = 0; i < _surface->n_knots_n(); ++i)
    _out << _surface->get_knot_n(i) << " ";
  _out << "\n";

  _out << "end";

  return true;
}

#endif

